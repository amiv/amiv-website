module.exports = {
  extends: [
    'airbnb-base',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
    'plugin:@next/next/recommended',
  ],
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  parserOptions: {
    requireConfigFile: false,
    ecmaVersion: 2021,
    parser: '@babel/eslint-parser',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'no-multi-str': 0,
    'no-underscore-dangle': 0,
    'no-console': 0,
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': [1, { peerDependencies: true }],
    'max-classes-per-file': [1, 2],
    camelcase: 0,
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'prettier/prettier': 'error',
  },
  plugins: ['jsx', 'react', 'prettier'],
  // Activate the resolver plugin, required to recognize the 'config' resolver
  settings: {
    react: {
      version: 'detect',
    },
    linkComponents: [
      // Components used as alternatives to <a> for linking, eg. <Link href={ url } />
      'Hyperlink',
      { name: 'Link', linkAttribute: 'href' },
    ],
    'import/resolver': {
      webpack: {},
      alias: [
        ['lib', './lib'],
        ['content', './content'],
        ['locales', './locales'],
        ['pages', './pages'],
        ['public', './public'],
        ['config', './config.js'],
      ],
    },
  },
}
