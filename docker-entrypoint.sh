#!/bin/bash

set -m
dir="${PWD}"
pages=()

# Cleanup function when script is interrupted (Ctrl+C)
function cleanup() {

  # Stop next server
  kill %1
  wait %1
  rm /tmp/server.log

  echo -e "\n** Next.js server has been stopped."
}

# Delete all html files of SSG pages (static pages with props)
function delete_html_files() {
  echo "** Deleting HTML files for statically rendered pages..."

  cd .next/server/pages

  shopt -s globstar
  for json_file in **/*.json; do
    file_without_extension="${json_file%.*}"
    html_file="${file_without_extension}.html"
    pages=(${pages[@]} "/${file_without_extension}")
    rm -f "${html_file}"
    echo " > Deleted file \"${html_file}\""
  done

  cd "$dir"
}

# Start the next server in the background
function start_nextjs_server() {
  echo "** Starting Next.js server..."
  yarn run start > /tmp/server.log &
  sleep 2
}

# Visit all pages where we deleted the server-side rendering
# before.
function visit_static_pages() {
  echo "** Visit static pages to trigger rerendering..."
  for path in "${pages[@]}"; do
    curl -s "http://localhost:3000${path}" > /dev/null
    echo " > Visited page \"${path}\""
  done
}

# Remove static rendered HTML pages where a JSON file exists.
# This ensures that environment variables given on startup are
# applied correctly to those static pages too. The pages are
# automatically rendered on the server-side on first visit.
# We therefore visit all those pages right on startup once.

delete_html_files
trap cleanup INT
start_nextjs_server
visit_static_pages

echo "** ------------------------------------------"
echo -e "** Next.js server is running!\n"

# Print output of server to stdout
tail -f /tmp/server.log
