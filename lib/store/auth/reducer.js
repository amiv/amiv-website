import {
  AUTH_LOGIN_PENDING,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_ERROR,
  AUTH_LOGOUT_PENDING,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_ERROR,
  AUTH_LOCALSTORAGE_LOADED,
  AUTH_KEY_TOKEN,
} from './constants'

const initialState = {
  isLoggedIn: false,
  token: null,
  session: null,
  user: null,
  isPending: false,
  error: null,
  isLocalStorageLoaded: false,
}

// Reducer performing the actions and updating the state object.
const reducer = (state, action) => {
  // eslint-disable-next-line no-param-reassign
  state = state || initialState

  switch (action.type) {
    case AUTH_LOGIN_PENDING:
    case AUTH_LOGOUT_PENDING:
      return {
        ...state,
        isPending: true,
        error: null,
      }
    case AUTH_LOGIN_SUCCESS:
      localStorage.setItem(AUTH_KEY_TOKEN, action.data.token)
      return {
        isLoggedIn: true,
        token: action.data.token,
        session: {
          ...action.data,
          user: action.data.user._id,
        },
        isPending: false,
        isLocalStorageLoaded: true,
        error: null,
      }
    case AUTH_LOGOUT_SUCCESS:
      localStorage.removeItem(AUTH_KEY_TOKEN)
      return initialState
    case AUTH_LOGIN_ERROR:
      localStorage.removeItem(AUTH_KEY_TOKEN)
    // eslint-disable-next-line no-fallthrough
    case AUTH_LOGOUT_ERROR:
      return {
        ...state,
        isPending: false,
        error: action.error,
      }
    case AUTH_LOCALSTORAGE_LOADED:
      return {
        ...state,
        isLocalStorageLoaded: true,
      }
    default:
      return state
  }
}

export default reducer
