import { randomString } from 'lib/utils'
import { getPublicConfig } from 'lib/utils/config'
import {
  AUTH_LOGIN_PENDING,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_ERROR,
  AUTH_LOGOUT_PENDING,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_ERROR,
  AUTH_LOCALSTORAGE_LOADED,
  AUTH_KEY_STATE,
  AUTH_KEY_TOKEN,
} from './constants'

/**
 * Initiate logout procedure
 */
const authLogout = () => (dispatch, getState) => {
  const { auth: { session } = {} } = getState()
  if (!session) {
    dispatch({ type: AUTH_LOGOUT_SUCCESS })
  } else {
    dispatch({
      types: [AUTH_LOGOUT_PENDING, AUTH_LOGOUT_SUCCESS, AUTH_LOGOUT_ERROR],
      resource: 'sessions',
      itemId: session._id,
      etag: session._etag,
      method: 'DELETE',
    })
  }
}

/**
 * Forcefully update local data to log out current user
 */
const authForcedLogout = () => ({ type: AUTH_LOGOUT_SUCCESS })

/**
 * Start OAuth login procedure and redirect to AMIV API OAuth2 page
 */
const authLoginStart = requestedPath => () => {
  const state = randomString(32)
  localStorage.setItem(AUTH_KEY_STATE, state)
  const { OAuthId, apiUrl } = getPublicConfig()

  const redirectPath = requestedPath || window.location.pathname

  const query = new URLSearchParams([
    ['response_type', 'token'],
    ['client_id', OAuthId],
    ['redirect_uri', window.location.origin + redirectPath],
    ['state', state],
  ])

  // Redirect to AMIV API OAuth2 page
  window.location.href = `${apiUrl}/oauth?${query.toString()}`
}

/**
 * Parse search parameters and try to log in with found token
 */
const authLoginBySearchParameters = searchParams => dispatch => {
  if (
    searchParams.get('state') &&
    searchParams.get('access_token') &&
    searchParams.get('state') === localStorage.getItem(AUTH_KEY_STATE)
  ) {
    const token = searchParams.get('access_token')

    dispatch({
      types: [AUTH_LOGIN_PENDING, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_ERROR],
      resource: 'sessions',
      itemId: token,
      method: 'GET',
      token,
      query: { embedded: { user: 1 } },
    })

    window.history.replaceState({}, document.title, window.location.pathname)
  }
}

/**
 * Read token from localStorage and try to log in if token found
 */
const authLoginByLocalStorageToken = () => (dispatch, getState) => {
  const { auth: { isLocalStorageLoaded } = {} } = getState()

  if (isLocalStorageLoaded) return

  dispatch({ type: AUTH_LOCALSTORAGE_LOADED })

  try {
    const token = localStorage.getItem(AUTH_KEY_TOKEN)

    if (token) {
      dispatch({
        types: [AUTH_LOGIN_PENDING, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_ERROR],
        resource: 'sessions',
        itemId: token,
        method: 'GET',
        token,
        query: { embedded: { user: 1 } },
      })
    }
  } catch (err) {
    // error while reading localStorage. Key might not exist yet.
  }
}

export {
  authLoginStart,
  authLoginBySearchParameters,
  authLoginByLocalStorageToken,
  authLogout,
  authForcedLogout,
}
