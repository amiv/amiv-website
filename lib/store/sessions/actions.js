import {
  SESSIONS,
  SESSIONS_PENDING,
  SESSIONS_SUCCESS,
  SESSIONS_ERROR,
  SESSIONS_CLEARED,
} from './constants'
import { ITEM_DELETED } from '../common/constants'
import Query from '../../utils/query'

/** Load all sessions of the current user */
const loadOtherSessions = () => (dispatch, getState) => {
  const user = getState().user.data

  const query = { where: { user: user._id } }

  return dispatch({
    types: [SESSIONS_PENDING, null, null],
    resource: SESSIONS,
    method: 'GET',
    query: Query.merge(query, { page: 1 }),
  })
    .then(response => {
      const pages = { 1: response._items }
      // save totalPages as a constant to avoid race condition with pages added during this
      // process
      const totalPages = Math.ceil(
        response._meta.total / response._meta.max_results
      )

      if (totalPages <= 1) {
        return response
      }

      // now fetch all the missing pages
      return Promise.all(
        Array.from(new Array(totalPages - 1), (x, i) => i + 2).map(pageNum =>
          dispatch({
            types: [null, null, null],
            resource: SESSIONS,
            method: 'GET',
            query: Query.merge(query, { page: pageNum }),
          }).then(response2 => {
            pages[pageNum] = response2._items
          })
        )
      ).then(() => {
        return {
          ...response,
          _items: [].concat(
            ...Object.keys(pages)
              .sort()
              .map(key => pages[key])
          ),
          _meta: {
            page: totalPages,
            max_results: response._meta.max_results,
            total: response._meta.total,
          },
        }
      })
    })
    .then(data => {
      dispatch({ type: SESSIONS_SUCCESS, data })
    })
    .catch(error => {
      dispatch({ type: SESSIONS_ERROR, error })
    })
}

const clearOtherSessions = () => (dispatch, getState) => {
  const sessions = getState().sessions.items
  const currentSession = getState().auth.session

  // Delete all sessions (except the currently used one)
  const promiseList = []
  sessions.forEach(session => {
    if (session._id !== currentSession._id) {
      promiseList.push(
        dispatch({
          types: [SESSIONS_PENDING, ITEM_DELETED(SESSIONS), null],
          resource: SESSIONS,
          itemId: session._id,
          etag: session._etag,
          method: 'DELETE',
        })
      )
    }
  })
  return Promise.all(promiseList)
    .then(() => dispatch({ type: SESSIONS_CLEARED }))
    .catch(error => {
      dispatch({ type: SESSIONS_ERROR, error })
      return error
    })
}

export { loadOtherSessions, clearOtherSessions }
