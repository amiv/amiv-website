import {
  SESSIONS,
  SESSIONS_PENDING,
  SESSIONS_SUCCESS,
  SESSIONS_ERROR,
  SESSIONS_CLEARED,
} from './constants'
import { ITEM_DELETED } from '../common/constants'
import { AUTH_LOGOUT_SUCCESS } from '../auth/constants'

const initialState = {
  items: null,
  isPending: false,
  error: null,
}

const reducer = (state, action) => {
  // eslint-disable-next-line no-param-reassign
  state = state || initialState

  switch (action.type) {
    case ITEM_DELETED(SESSIONS):
      return {
        ...state,
        items: state.items.filter(item => item._id !== action.id),
      }
    case SESSIONS_PENDING:
      return {
        ...state,
        isPending: true,
      }
    case SESSIONS_SUCCESS:
      return {
        ...state,
        items: action.data._items,
        isPending: false,
      }
    case SESSIONS_ERROR:
      return {
        ...state,
        isPending: false,
        error: action.error,
      }
    case SESSIONS_CLEARED:
      return {
        ...state,
        isPending: false,
        error: null,
        items: [],
      }
    case AUTH_LOGOUT_SUCCESS:
      return { ...state, items: null }
    default:
      return state
  }
}

export default reducer
