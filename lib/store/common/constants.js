export const LIST_PENDING = resource => `${resource}_LIST_PENDING`
export const LIST_SUCCESS = resource => `${resource}_LIST_SUCCESS`
export const LIST_ERROR = resource => `${resource}_EVENTS_LIST_ERROR`

export const SET_QUERY = resource => `${resource}_SET_QUERY`
export const SET_FILTER_VALUE = resource => `${resource}_SET_FILTER_VALUE`
export const SET_FILTER_VALUES = resource => `${resource}_SET_FILTER_VALUES`
export const RESET_FILTER_VALUES = resource => `${resource}_RESET_FILTER_VALUES`

export const ITEM_PENDING = resource => `${resource}_ITEM_PENDING`
export const ITEM_SUCCESS = resource => `${resource}_ITEM_SUCCESS`
export const ITEM_DELETED = resource => `${resource}_ITEM_DELETED`
export const ITEM_ERROR = resource => `${resource}_ITEM_ERROR`
