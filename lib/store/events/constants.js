export const EVENTS = 'events'

export const EVENTS_SIGNUP_PENDING = 'EVENTS_SIGNUP_PENDING'
export const EVENTS_SIGNUP_SUCCESS = 'EVENTS_SIGNUP_SUCCESS'
export const EVENTS_SIGNUP_EMAIL_SUCCESS = 'EVENTS_SIGNUP_EMAIL_SUCCESS'
export const EVENTS_SIGNUP_DELETED = 'EVENTS_SIGNUP_DELETED'
export const EVENTS_SIGNUP_ERROR = 'EVENTS_SIGNUP_ERROR'
