import {
  EVENTS,
  EVENTS_SIGNUP_PENDING,
  EVENTS_SIGNUP_SUCCESS,
  EVENTS_SIGNUP_EMAIL_SUCCESS,
  EVENTS_SIGNUP_DELETED,
  EVENTS_SIGNUP_ERROR,
} from './constants'

import { AUTH_LOGOUT_SUCCESS } from '../auth/constants'
import generateReducer from '../common/reducer'

const lists = {
  frontpageUpcoming: {
    useQuery: false,
    createBaseQuery: now => ({
      max_results: 12,
      sort: ['-priority', '-time_advertising_start'],
      where: {
        show_website: true,
        time_start: { $gt: now },
        time_advertising_start: { $lt: now },
        time_advertising_end: { $gt: now },
        img_poster: { $ne: null },
      },
    }),
  },
  frontpagePast: {
    useQuery: false,
    createBaseQuery: now => ({
      max_results: 6,
      sort: ['-time_start', '-time_advertising_start'],
      where: {
        show_website: true,
        time_advertising_start: { $lt: now },
        img_poster: { $ne: null },
        $or: [{ time_start: null }, { time_start: { $lt: now } }],
      },
    }),
  },
  upcomingWithOpenRegistration: {
    useQuery: true,
    createBaseQuery: now => ({
      where: {
        show_website: true,
        time_advertising_start: { $lt: now },
        time_register_start: { $lt: now },
        time_register_end: { $gt: now },
      },
    }),
  },
  upcomingWithoutOpenRegistration: {
    useQuery: true,
    createBaseQuery: now => ({
      where: {
        show_website: true,
        time_start: { $gt: now },
        time_advertising_start: { $lt: now },
        $or: [
          { time_register_start: null },
          { time_register_end: null },
          { time_register_end: { $lt: now } },
          { time_register_start: { $gt: now } },
        ],
      },
    }),
  },
  past: {
    useQuery: true,
    createBaseQuery: now => ({
      sort: ['-time_start', '-time_advertising_start'],
      max_results: 10,
      where: {
        show_website: true,
        time_advertising_start: { $lt: now },
        $or: [{ time_start: null }, { time_start: { $lt: now } }],
      },
    }),
  },
}

const initialFilterValues = {
  search: '',
  price: 'all',
  restrictions: 'members_only',
}

const baseReducer = generateReducer(EVENTS, lists, initialFilterValues)

// Extend basic resource reducer with additional actions for signup handling.
const reducer = (state, action) => {
  // eslint-disable-next-line no-param-reassign
  state = state || { ...baseReducer.createInitialState(), signups: {} }

  switch (action.type) {
    case EVENTS_SIGNUP_PENDING:
      return {
        ...state,
        signups: {
          [action.eventId]: {
            ...state.signups[action.eventId],
            isPending: true,
            error: false,
          },
        },
      }
    case EVENTS_SIGNUP_SUCCESS:
      // eslint-disable-next-line no-case-declarations
      let data = null

      if (action.data._items && action.data._items.length > 0) {
        ;[data] = action.data._items // use first item of array
      } else if (action.data._id) {
        data = action.data
      }

      return {
        ...state,
        signups: {
          ...state.signups,
          [action.eventId]: {
            data,
            isPending: false,
            error: false,
          },
        },
      }
    case EVENTS_SIGNUP_EMAIL_SUCCESS:
      return {
        ...state,
        signups: {
          ...state.signups,
          [action.eventId]: undefined,
        },
      }
    case EVENTS_SIGNUP_DELETED:
      return {
        ...state,
        signups: {
          ...state.signups,
          [action.eventId]: {
            data: null,
            isPending: false,
            error: false,
          },
        },
      }
    case EVENTS_SIGNUP_ERROR:
      return {
        ...state,
        signups: {
          [action.eventId]: {
            ...state.signups[action.eventId],
            isPending: false,
            error: action.error,
          },
        },
      }
    case AUTH_LOGOUT_SUCCESS:
      return { ...state, signups: {} }
    default:
      return baseReducer(state, action)
  }
}

export default reducer
