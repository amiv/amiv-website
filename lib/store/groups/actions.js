import {
  GROUPS,
  GROUPMEMBERSHIPS,
  GROUPS_MEMBERSHIPS_PENDING,
  GROUPS_MEMBERSHIPS_SUCCESS,
  GROUPS_MEMBERSHIPS_ERROR,
  GROUPS_PUBLIC_PENDING,
  GROUPS_PUBLIC_SUCCESS,
  GROUPS_PUBLIC_ERROR,
  GROUPMEMBERSHIPS_ENROLLED,
  GROUPMEMBERSHIPS_WITHDRAWN,
} from './constants'

/**
 * Load all public groups
 */
const loadPublicGroups = () => ({
  types: [GROUPS_PUBLIC_PENDING, GROUPS_PUBLIC_SUCCESS, GROUPS_PUBLIC_ERROR],
  resource: GROUPS,
  method: 'GET',
  query: { where: { allow_self_enrollment: true } },
})

/**
 * Load all group memberships of the current user
 */
const loadMemberships = () => (dispatch, getState) => {
  const { auth: { session } = {} } = getState()

  return dispatch({
    types: [
      GROUPS_MEMBERSHIPS_PENDING,
      GROUPS_MEMBERSHIPS_SUCCESS,
      GROUPS_MEMBERSHIPS_ERROR,
    ],
    resource: GROUPMEMBERSHIPS,
    method: 'GET',
    query: {
      where: { user: session ? session.user : null },
      embedded: { group: 1 },
    },
  })
}

/**
 * Enroll the current user to the given group.
 * @param {object} group group object from API
 */
const enrollToGroup = group => (dispatch, getState) => {
  const { auth: { session } = {} } = getState()

  return dispatch({
    types: [
      GROUPS_MEMBERSHIPS_PENDING,
      { type: GROUPMEMBERSHIPS_ENROLLED, group },
      GROUPS_MEMBERSHIPS_ERROR,
    ],
    resource: GROUPMEMBERSHIPS,
    method: 'POST',
    data: {
      group: group._id,
      user: session.user,
    },
  })
}

/**
 * Withdraw the given membership.
 * @param {object} membership groupmembership object
 */
const withdrawFromGroup = membership => ({
  types: [null, { type: GROUPMEMBERSHIPS_WITHDRAWN, id: membership._id }, null],
  resource: GROUPMEMBERSHIPS,
  method: 'DELETE',
  itemId: membership._id,
  etag: membership._etag,
})

export { loadPublicGroups, loadMemberships, enrollToGroup, withdrawFromGroup }
