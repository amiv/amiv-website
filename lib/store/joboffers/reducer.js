import { JOBOFFERS } from './constants'
import generateReducer from '../common/reducer'

const lists = {
  frontpage: {
    useQuery: false,
    createBaseQuery: now => ({
      max_results: 6,
      where: {
        time_end: { $gte: now },
        show_website: true,
      },
      sort: ['-_created'],
    }),
  },
  default: {
    useQuery: true,
    createBaseQuery: now => ({
      where: {
        time_end: { $gte: now },
        show_website: true,
      },
      sort: ['-_created'],
    }),
  },
}

const initialFilterValues = {
  search: '',
}

const reducer = generateReducer(JOBOFFERS, lists, initialFilterValues)

export default reducer
