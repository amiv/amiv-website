import { configureStore, combineReducers } from '@reduxjs/toolkit'
import thunk from 'redux-thunk'
import { createWrapper, HYDRATE } from 'next-redux-wrapper'

import { isBrowser } from 'lib/utils'

import apiMiddleware from './apiMiddleware'
import authReducer from './auth/reducer'
import userReducer from './user/reducer'
import groupsReducer from './groups/reducer'
import sessionsReducer from './sessions/reducer'
import eventsReducer from './events/reducer'
import joboffersReducer from './joboffers/reducer'
import studydocumentsReducer from './studydocuments/reducer'

// preloadedState will be passed in by the plugin
const initStore = () => {
  const rootReducer = (state, action) => {
    if (action.type === HYDRATE) {
      const nextState = {
        ...state, // use previous state
        ...action.payload, // apply delta from hydration
      }

      if (typeof window !== 'undefined' && state.router) {
        // preserve router value on client side navigation
        nextState.router = state.router
      }

      return nextState
    }

    return combineReducers({
      events: eventsReducer,
      joboffers: joboffersReducer,
      studydocuments: studydocumentsReducer,
      auth: authReducer,
      user: userReducer,
      groups: groupsReducer,
      sessions: sessionsReducer,
    })(state, action)
  }

  const middleware = [apiMiddleware, thunk]

  // In development, use the browser's Redux dev tools extension if installed
  const isDevelopment = process.env.NODE_ENV === 'development'
  const applyDevTools = isDevelopment && isBrowser() && window.devToolsExtension

  return configureStore({
    reducer: rootReducer,
    middleware,
    devTools: applyDevTools,
  })
}

export const wrapper = createWrapper(initStore)
