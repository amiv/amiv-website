import {
  STUDYDOCUMENTS,
  STUDYDOCUMENTS_EDITFORM_PENDING,
  STUDYDOCUMENTS_EDITFORM_SUCCESS,
  STUDYDOCUMENTS_EDITFORM_ERROR,
  STUDYDOCUMENTS_QUICKFILTER_PENDING,
  STUDYDOCUMENTS_QUICKFILTER_SUCCESS,
  STUDYDOCUMENTS_QUICKFILTER_ERROR,
  STUDYDOCUMENTS_QUICKFILTER_RESET,
  STUDYDOCUMENTS_QUICKFILTER_SET_FILTER_VALUES,
  STUDYDOCUMENTS_QUICKFILTER_STEP_SEMESTER,
} from './constants'
import { LIST_SUCCESS } from '../common/constants'
import generateReducer from '../common/reducer'

const lists = {
  default: {
    useQuery: true,
    additionalProps: { summary: {} },
    preservedAdditionalProps: ['summary'],
    createBaseQuery: () => ({
      sort: ['lecture', '-course_year', 'type', 'title', 'author'],
    }),
  },
}

const initialFilterValues = {
  search: '',
  department: [],
  semester: [],
  lecture: [],
  professor: [],
  type: [],
}

const baseReducer = generateReducer(STUDYDOCUMENTS, lists, initialFilterValues)

// Extend basic resource reducer with additional actions.
const reducer = (state, action) => {
  // eslint-disable-next-line no-param-reassign
  state = state || {
    ...baseReducer.createInitialState(),
    quickFilter: {
      filterValues: initialFilterValues,
      summary: {},
      step: STUDYDOCUMENTS_QUICKFILTER_STEP_SEMESTER,
      isPending: false,
      error: null,
    },
    editForm: {
      summary: {},
      isPending: false,
      error: null,
    },
  }

  switch (action.type) {
    case STUDYDOCUMENTS_EDITFORM_PENDING:
      return {
        ...state,
        editForm: {
          ...state.editForm,
          isPending: true,
          error: null,
        },
      }
    case STUDYDOCUMENTS_EDITFORM_SUCCESS:
      return {
        ...state,
        editForm: {
          ...state.editForm,
          summary: action.data._summary,
          isPending: false,
          error: null,
        },
      }
    case STUDYDOCUMENTS_EDITFORM_ERROR:
      return {
        ...state,
        editForm: {
          ...state.editForm,
          isPending: false,
          error: action.error,
        },
      }
    case STUDYDOCUMENTS_QUICKFILTER_PENDING:
      return {
        ...state,
        quickFilter: {
          ...state.quickFilter,
          isPending: true,
          error: null,
        },
      }
    case STUDYDOCUMENTS_QUICKFILTER_SUCCESS:
      return {
        ...state,
        quickFilter: {
          ...state.quickFilter,
          summary: action.data._summary,
          isPending: false,
          error: null,
        },
      }
    case STUDYDOCUMENTS_QUICKFILTER_ERROR:
      return {
        ...state,
        quickFilter: {
          ...state.quickFilter,
          isPending: false,
          error: action.error,
        },
      }
    case STUDYDOCUMENTS_QUICKFILTER_RESET:
      return {
        ...state,
        quickFilter: {
          ...state.quickFilter,
          summary: null,
          step: STUDYDOCUMENTS_QUICKFILTER_STEP_SEMESTER,
          filterValues: initialFilterValues,
          query: {},
        },
      }
    case STUDYDOCUMENTS_QUICKFILTER_SET_FILTER_VALUES:
      return {
        ...state,
        quickFilter: {
          ...state.quickFilter,
          step: action.step || state.quickFilter.step,
          filterValues: action.filterValues,
          query: action.query,
        },
      }
    case LIST_SUCCESS(STUDYDOCUMENTS):
      // Extends an existing reducer action
      // Use existing summary values when a filter is applied to that field.
      // eslint-disable-next-line no-case-declarations
      const newState = {
        ...state,
        [action.listName]: {
          ...state[action.listName],
          summary: {
            ...Object.assign(
              {},
              ...Object.keys(state[action.listName].summary).map(key => {
                if (
                  (state.filterValues[key] instanceof String &&
                    state.filterValues[key] !== '') ||
                  (Array.isArray(state.filterValues[key]) &&
                    state.filterValues[key].length > 0)
                ) {
                  return { [key]: state[action.listName].summary[key] }
                }
                return {}
              }),
              ...Object.keys(action.data._summary || {}).map(key => {
                if (
                  state.filterValues[key] === '' ||
                  (Array.isArray(state.filterValues[key]) &&
                    state.filterValues[key].length === 0)
                ) {
                  return { [key]: action.data._summary[key] }
                }
                return {}
              })
            ),
          },
        },
      }
      return baseReducer(newState, action)
    default:
      return baseReducer(state, action)
  }
}

export default reducer
