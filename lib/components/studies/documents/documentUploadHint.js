import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import { makeStyles } from '@mui/styles'
import Alert from '@mui/material/Alert'

import Link from '../../general/link'

const useStyles = makeStyles(
  {
    root: {
      margin: '2em 0',
    },
    link: {
      cursor: 'pointer',
    },
    dialogContent: {
      padding: '0 24px 16px 24px',
    },
  },
  { name: 'studydocumentsUploadHint' }
)

const StudydocumentsUploadHint = ({ className, ...props }) => {
  const classes = useStyles()

  return (
    <Alert
      className={[classes.root, className].join(' ')}
      severity="info"
      {...props}
    >
      <div>
        <b>
          <FormattedMessage id="studydocuments.newcollection" />
        </b>
        &nbsp;
        <Link href="https://exams.amiv.ethz.ch" className={classes.link}>
          <FormattedMessage id="studydocuments.uploadCatchphrase" />
        </Link>
      </div>
    </Alert>
  )
}

StudydocumentsUploadHint.propTypes = {
  /** @ignore */
  className: PropTypes.string,
}

export default StudydocumentsUploadHint
