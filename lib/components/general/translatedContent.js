import React from 'react'
import { marked } from 'marked'
import escape from 'html-escape'
import PropTypes from 'prop-types'
import { useIntl } from 'react-intl'
import { Parser as HtmlToReactParser } from 'html-to-react'

import TranslatedAlert from './translatedAlert'

const TranslatedContent = ({
  component: Component,
  content,
  parseMarkdown,
  noEscape,
  noHint,
  ...props
}) => {
  const intl = useIntl()

  let message = ''
  let hint = null

  if (intl.locale in content && content[intl.locale]) {
    message = content[intl.locale]
  } else if (intl.defaultLocale in content && content[intl.defaultLocale]) {
    message = content[intl.defaultLocale]
    hint = <TranslatedAlert shownLanguage={intl.defaultLocale} />
  } else {
    const keys = Object.keys(content)
    if (keys.length > 0) {
      const language = Object.keys(content)[0]
      message = content[language]
      hint = <TranslatedAlert shownLanguage={language} />
    }
  }

  if (!noEscape) {
    message = escape(message)
  }

  const htmlToReactParser = new HtmlToReactParser()

  return (
    <Component {...props}>
      {!noHint && hint}
      {htmlToReactParser.parse(
        parseMarkdown && message ? marked.parse(message) : message
      )}
    </Component>
  )
}

TranslatedContent.propTypes = {
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.elementType]),
  /** Content for all available languages */
  content: PropTypes.object.isRequired,
  /** Specifies to parse markdown */
  parseMarkdown: PropTypes.bool,
  /**
   * Specifies whether to escape the content.
   *
   * Only use this option when static markdown is used or the
   * content is REALLY trusted!
   */
  noEscape: PropTypes.bool,
  /**
   * Specifies whether to hide the alert when another language
   * is displayed instead.
   */
  noHint: PropTypes.bool,
}

TranslatedContent.defaultProps = {
  component: 'div',
}

export default TranslatedContent
