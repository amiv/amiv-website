import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'
import Img from 'next/image'
import { makeStyles } from '@mui/styles'
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline'
import { Typography } from '@mui/material'

const useStyles = makeStyles(
  theme => ({
    root: {
      width: '100%',
      backgroundColor: theme.palette.common.grey,
      position: 'relative',
      verticalAlign: 'middle',
    },
    imgContainer: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      overflow: 'hidden',
      '& > span': {
        top: '50%',
        left: '50%',
        transform: 'translateX(-50%) translateY(-50%)',
      },
    },
    contain: {
      objectFit: 'contain',
    },
    cover: {
      objectFit: 'cover',
    },
    none: {
      objectFit: 'none',
    },
    error: {
      position: 'absolute !important',
      maxWidth: '100%',
      maxHeight: '100%',
      top: '50% !important',
      left: '50% !important',
      transform: 'translateX(-50%) translateY(-50%)',

      '& > *': {
        display: 'inline-block',
        verticalAlign: 'middle',
        marginLeft: '.5em',
      },
    },
  }),
  { name: 'image' }
)

const Image = ({
  src,
  ratioX,
  ratioY,
  alt,
  className,
  objectFit,
  priority,
  sizes,
  onLoadingComplete,
  onLoad,
  onError,
  loadingPlaceholder,
  ...props
}) => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const classes = useStyles()

  const hasRatio = ratioX && ratioY
  const ratio = hasRatio ? (ratioY / ratioX) * 100 : 1

  const handleImageLoaded = () => {
    setError(false)
    setLoading(false)
    if (onLoadingComplete) {
      onLoadingComplete()
    }
  }

  const handleImageLoad = () => {
    setLoading(true)
    setError(false)
    if (onLoad) {
      onLoad()
    }
  }

  const handleImageError = () => {
    setError(true)
    setLoading(false)
    if (onError) {
      onError()
    }
  }

  useEffect(() => {
    setLoading(true)
  })

  if (!src) {
    return (
      <div
        className={[classes.root, className].join(' ')}
        style={hasRatio && { paddingBottom: `${ratio}%` }}
        {...props}
      >
        <div className={classes.error}>
          <ErrorOutlineIcon />
          <Typography>
            <FormattedMessage id="missing.image" />
          </Typography>
        </div>
      </div>
    )
  }

  if (error) {
    return (
      <div
        className={[classes.root, className].join(' ')}
        style={hasRatio && { paddingBottom: `${ratio}%` }}
        {...props}
      >
        <div className={classes.error}>
          <ErrorOutlineIcon />
          <Typography>
            <FormattedMessage id="error.image" />
          </Typography>
        </div>
      </div>
    )
  }

  return (
    <div
      className={[classes.root, className].join(' ')}
      style={hasRatio && { paddingBottom: `${ratio}%` }}
      {...props}
    >
      {loading && loadingPlaceholder}
      <div className={classes.imgContainer}>
        <Img
          className={classes[objectFit]}
          fill={true}
          src={src}
          alt={alt}
          onLoadingComplete={handleImageLoaded}
          onLoad={handleImageLoad}
          onError={handleImageError}
          priority={priority}
          sizes={sizes}
        />
      </div>
    </div>
  )
}

Image.propTypes = {
  /** Target URL */
  src: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  /** Alternative text shown when image could not be loaded */
  alt: PropTypes.string,
  /** Priority=true will load first */
  priority: PropTypes.bool,
  /** Ratio number for x direction. RatioY is also required if this value is set. */
  ratioX: PropTypes.number,
  /** Ratio number for y direction. RatioX is also required if this value is set. */
  ratioY: PropTypes.number,
  /** Value for object-fit CSS style */
  objectFit: PropTypes.oneOf(['contain', 'cover', 'none']),
  /** Hint for preparing the srcset with appropriate sizes. */
  sizes: PropTypes.string,
  /** Callback when loading was finished */
  onLoadingComplete: PropTypes.func,
  /** Callback when loading is started */
  onLoad: PropTypes.func,
  /** Callback when image could not be loaded */
  onError: PropTypes.func,
  /** Optional placeholder element while image is loading */
  loadingPlaceholder: PropTypes.element,
  /** @ignore */
  className: PropTypes.string,
}

Image.defaultProps = {
  objectFit: 'contain',
  priority: false,
}

export default Image
