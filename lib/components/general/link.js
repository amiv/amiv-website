import React from 'react'
import PropTypes from 'prop-types'
import { Link as WrapperLink } from 'amiv-react-components'
import NextLink from 'next/link'

const Link = ({ children, ...props }) => {
  return (
    <WrapperLink {...props} component={NextLink}>
      {children}
    </WrapperLink>
  )
}

Link.propTypes = {
  /** Target URL */
  href: PropTypes.string.isRequired,
  /** Target language */
  language: PropTypes.string,
  /** Text color */
  color: PropTypes.string,
  /** Content of the link (text) */
  children: PropTypes.node.isRequired,
}

Link.defaultProps = {
  color: 'secondary',
}

export default Link
