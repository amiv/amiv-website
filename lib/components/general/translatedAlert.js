import React from 'react'
import PropTypes from 'prop-types'
import { useIntl, FormattedMessage } from 'react-intl'
import Alert from '@mui/material/Alert'
import TranslateIcon from '@mui/icons-material/Translate'

const TranslatedAlert = ({ shownLanguage, ...props }) => {
  const intl = useIntl()

  return (
    <Alert icon={<TranslateIcon />} severity="info" {...props}>
      <FormattedMessage id="error.translationUnavailable" />
      &nbsp;
      <FormattedMessage
        id="error.shownLanguage"
        values={{
          shownLanguage: intl.formatMessage({
            id: `language.${shownLanguage}`,
          }),
        }}
      />
    </Alert>
  )
}

TranslatedAlert.propTypes = {
  /** Shown language */
  shownLanguage: PropTypes.string.isRequired,
}

export default TranslatedAlert
