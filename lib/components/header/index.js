import React, { useState, useEffect } from 'react'
import { useIntl } from 'react-intl'
import PropTypes from 'prop-types'
import Img from 'next/image'
import { makeStyles } from '@mui/styles'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Collapse from '@mui/material/Collapse'
import NoSsr from '@mui/material/NoSsr'
import { BurgerIcon, HideOnScroll } from 'amiv-react-components'

import { getPublicConfig } from 'lib/utils/config'
import { Stack } from '@mui/material'
import Link from '../general/link'
import LanguageSelector from './languageSelector'
import MainMenu from './mainMenu'
import ProfileMenu from './profileMenu'

let persistedMobileMenuShowing = false

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.common.header,
    zIndex: theme.zIndex.appBar,
    transition:
      'background .125s ease, transform 225ms cubic-bezier(0, 0, 0.2, 1) 0ms !important',
    '& > div': {
      display: 'grid',
      margin: '0 auto',
      width: '100%',
      maxWidth: theme.shape.maxContentWidth,
      [theme.breakpoints.up('md')]: {
        gridTemplateColumns: 'auto 1fr',
        gridTemplateAreas: "'logo navigation'",
      },
      [theme.breakpoints.down('md')]: {
        gridTemplateColumns: '1fr auto',
        gridTemplateAreas: "'logo mobile-menu' 'navigation navigation'",
        height: 'auto',
        maxHeight: theme.shape.headerHeight,
        transition: 'max-height ease-in-out 500ms',
        alignItems: 'center',
      },
    },
  },
  nav: {
    gridArea: 'navigation',
    width: '100%',
    margin: '0 auto',
    display: 'grid',
    gridGap: '1em',
    fontSize: '1.3em',
    [theme.breakpoints.up('md')]: {
      height: 'auto !important',
      visibility: 'visible',
    },
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
    [theme.breakpoints.up('md')]: {
      gridTemplateColumns: '1fr auto auto auto',
      gridTemplateAreas: "'main-navigation profile language-selector'",
      maxWidth: theme.shape.maxContentWidth,
    },
  },
  navMobile: {
    gridArea: 'navigation',
    width: '100%',
    margin: '0 auto',
    display: 'none',
    fontSize: '1.3em',
    [theme.breakpoints.down('md')]: {
      display: 'block',
    },

    '& > div > div': {
      display: 'grid',
      gridGap: '1em',
      [theme.breakpoints.down('md')]: {
        padding: '0 1em',
        backgroundColor: theme.palette.common.header,
        gridTemplateColumns: '1fr',
        gridTemplateAreas: "'main-navigation' 'profile' 'language-selector'",
        height: 'auto',
        maxHeight: `calc(100vh - ${theme.shape.headerHeight}px)`,
        overflowY: 'auto',
        alignItems: 'center',
      },
    },
  },
  logo: {
    gridArea: 'logo',
    height: theme.shape.headerHeight,
    alignItems: 'center',
  },
  logoImage: {
    width: theme.shape.headerHeight * 2.3,
    display: 'block',
    [theme.breakpoints.only('md')]: {
      display: 'none',
    },
  },
  logoWheelImage: {
    width: theme.shape.headerHeight,
    padding: '11px',
    display: 'none',
    [theme.breakpoints.only('md')]: {
      display: 'block',
    },
  },
  mobileMenuIcon: {
    gridArea: 'mobile-menu',
    height: theme.shape.headerHeight,
    lineHeight: `${theme.shape.headerHeight}px`,
    display: 'none',
    textAlign: 'right',
    verticalAlign: 'middle',
    [theme.breakpoints.down('md')]: {
      display: 'block',
    },
    '& div div': {
      backgroundColor: theme.palette.common.amivred,
    },
  },
  mainMenu: {
    gridArea: 'main-navigation',
  },
  profileMenu: {
    gridArea: 'profile',
  },
  languageSelector: {
    gridArea: 'language-selector',
  },
}))

const Header = ({ className }) => {
  const [showMobileMenu, setShowMobileMenu] = useState(
    persistedMobileMenuShowing
  )
  const [persistedDataUpdated, setPersistedDataUpdated] = useState(false)
  const intl = useIntl()
  const classes = useStyles()
  const { title: siteTitle, bannerText } = getPublicConfig()

  const title = intl.formatMessage({
    id: siteTitle,
  })

  useEffect(() => {
    if (persistedDataUpdated) return
    persistedMobileMenuShowing = false
    setPersistedDataUpdated(true)
  })

  const handleMobileMenuIconClick = () => {
    // reset persisted value
    persistedMobileMenuShowing = false
    setShowMobileMenu(!showMobileMenu)
  }

  const handleMobileMenuClose = () => {
    // reset persisted value
    persistedMobileMenuShowing = false
    setShowMobileMenu(!showMobileMenu)
  }

  const handleKeyPress = event => {
    if (event.keyCode === 27) {
      persistedMobileMenuShowing = false
      setShowMobileMenu(false)
    }
  }

  const preventMobileMenuClosing = () => {
    persistedMobileMenuShowing = true
  }

  const menuComponents = (
    <React.Fragment>
      <MainMenu
        className={classes.mainMenu}
        preventMobileMenuClosing={preventMobileMenuClosing}
        closeMobileMenu={handleMobileMenuClose}
      />
      <NoSsr>
        <ProfileMenu
          className={classes.profileMenu}
          preventMobileMenuClosing={preventMobileMenuClosing}
        />
      </NoSsr>
      <LanguageSelector className={classes.languageSelector} />
    </React.Fragment>
  )

  return (
    <HideOnScroll enabled={!showMobileMenu}>
      <Stack>
        <AppBar
          className={[className, classes.root].join(' ')}
          position="sticky"
          color="secondary"
          onKeyDown={handleKeyPress}
        >
          <div>
            <Link href="/" className={classes.logo} aria-label={title}>
              <Img
                className={classes.logoImage}
                src="/logos/amiv.svg"
                alt={title}
                width={202}
                height={88}
              />
              <Img
                className={classes.logoWheelImage}
                src="/logos/amiv-wheel.svg"
                alt={title}
                width={88}
                height={88}
              />
            </Link>
            <div className={classes.mobileMenuIcon}>
              <BurgerIcon
                active={showMobileMenu}
                onClick={handleMobileMenuIconClick}
              />
            </div>
            <nav className={classes.nav}>{menuComponents}</nav>
            <Collapse
              component={Box}
              boxShadow={8}
              in={showMobileMenu}
              className={classes.navMobile}
            >
              {menuComponents}
            </Collapse>
          </div>
        </AppBar>
        {bannerText && (
          <Box
            bgcolor="#e8462b"
            sx={{
              padding: 1,
              textAlign: 'center',
              textTransform: 'uppercase',
              fontWeight: 'bold',
            }}
          >
            {bannerText}
          </Box>
        )}
      </Stack>
    </HideOnScroll>
  )
}

Header.propTypes = {
  className: PropTypes.string,
}

export default Header
