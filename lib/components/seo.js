/**
 * SEO component that controls the meta properties in the
 * HTML document's header.
 */

import React from 'react'
import PropTypes from 'prop-types'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useIntl } from 'react-intl'

import { getPublicConfig } from 'lib/utils/config'

function SEO({ description, title, meta, noindex }) {
  const { locale, locales, asPath: currentPath } = useRouter()
  const {
    title: siteTitle,
    description: defaultDescription,
    author,
  } = getPublicConfig()
  const { formatMessage } = useIntl()
  const f = id => formatMessage({ id })

  const translatedTitle = title ? f(title) : undefined
  const translatedSiteTitle = f(siteTitle)
  const translatedDescription = f(description || defaultDescription)
  const finalTitle = translatedTitle
    ? `${translatedTitle} | ${translatedSiteTitle}`
    : translatedSiteTitle

  return (
    <Head>
      <title>{finalTitle}</title>
      <meta name="description" content={translatedDescription} />
      <meta property="og:type" content="website" />
      <meta property="og:title" content={finalTitle} />
      <meta property="og:description" content={translatedDescription} />
      <meta property="og:site_name" content={translatedSiteTitle} />
      <meta property="twitter:card" content="summary" />
      <meta property="twitter:creator" content={author} />
      <meta property="twitter:title" content={finalTitle} />
      <meta property="twitter:description" content={translatedDescription} />
      {noindex && <meta property="robots" content="noindex" />}
      {meta}
      {locales
        .filter(
          otherLocale => otherLocale !== 'catchAll' && otherLocale !== locale
        )
        .map(otherLocale => {
          const link = `/${otherLocale}${currentPath}`

          return (
            <link
              rel="alternate"
              href={link}
              hrefLang={otherLocale}
              key={otherLocale}
            />
          )
        })}
    </Head>
  )
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string,
  noindex: PropTypes.bool,
}

SEO.defaultProps = {
  meta: [],
  noindex: false,
}

export default SEO
