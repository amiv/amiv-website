import React from 'react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'

const EventPrice = ({ price, ...props }) => {
  if (price) {
    const cents = price % 100
    return (
      <div {...props}>
        {`CHF ${`${Math.floor(price / 100)}.${
          cents === 0 ? '–' : (cents < 10 ? '0' : '') + cents
        }`}`}
      </div>
    )
  }
  return (
    <div {...props}>
      <FormattedMessage id="events.free" />
    </div>
  )
}

EventPrice.propTypes = {
  /** Price of the event */
  price: PropTypes.number,
}

export default EventPrice
