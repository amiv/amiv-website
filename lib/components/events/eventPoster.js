import React from 'react'
import { useIntl } from 'react-intl'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@mui/styles'
import { Box, Skeleton } from '@mui/material'
import { Spinner } from 'amiv-react-components'

import { getPublicConfig } from 'lib/utils/config'
import { translateMessage } from 'lib/utils'
import Img from '../general/image'
import Link from '../general/link'

const useStyles = makeStyles(
  {
    root: {
      borderRadius: '4px',
      overflow: 'hidden',
    },
    placeholderText: {
      textAlign: 'center',
      width: '65%',
      '& > div': {
        display: 'inline-block',
        height: '2.5em',
      },
    },
  },
  { name: 'eventPoster' }
)

const EventPoster = ({ event, elevation, className, priority, ...props }) => {
  const { apiUrl } = getPublicConfig()
  const intl = useIntl()
  const classes = useStyles()
  const theme = useTheme()

  const loadingPlaceholder = (
    <React.Fragment>
      <Skeleton
        width="100%"
        height="100%"
        variant="rectangular"
        animation="pulse"
      />
      <Spinner centered background={theme.palette.common.white} elevation={2} />
    </React.Fragment>
  )

  const title = event
    ? translateMessage({ en: event.title_en, de: event.title_de }, intl)
    : ''

  const imageTag = (
    <Img
      src={event ? `${apiUrl}${event.img_poster.file}` : null}
      ratioX={1}
      ratioY={1.41421356237}
      alt={title}
      priority={priority}
      loadingPlaceholder={loadingPlaceholder}
    />
  )

  return (
    <Box boxShadow={2} className={[classes.root].join(' ')} {...props}>
      {event ? <Link href={`/events/${event._id}`}>{imageTag}</Link> : imageTag}
    </Box>
  )
}

EventPoster.propTypes = {
  /** Event object */
  event: PropTypes.object,
  /** Specifies the size of the shadow around the card */
  elevation: PropTypes.number,
  /** Priority=true will load first */
  priority: PropTypes.bool,
  /** Additional class for the outermost box */
  className: PropTypes.string,
}

EventPoster.defaultProps = {
  elevation: 0,
  priority: false,
}

export default EventPoster
