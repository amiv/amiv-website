import React from 'react'
import { useRouter } from 'next/router'

import EventDetails from 'lib/components/events/details'
import EventSummary from 'lib/components/events/summary'
import Layout from 'lib/components/layout'
import { Card, NoSsr } from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles(
  () => ({
    root: {
      textAlign: 'center',
      padding: '2em 0',
    },
    card: {
      padding: '16px',
    },
  }),
  { name: 'eventsignup' }
)

export const SignUpPage = () => {
  const classes = useStyles()
  const {
    query: { eventId },
  } = useRouter()

  // disable SSR for the event details component because it uses the window object
  return (
    <Layout className={classes.root} seoProps={{ title: 'events.title' }}>
      <Card className={classes.card}>
        <EventSummary eventId={eventId} />
        <NoSsr>
          <EventDetails eventId={eventId} showSignupForm={true} />
        </NoSsr>
      </Card>
    </Layout>
  )
}
