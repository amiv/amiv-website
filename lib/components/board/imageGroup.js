import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@mui/styles'
import { Typography } from '@mui/material'

import Image from '../general/image'

const useStyles = makeStyles(
  theme => ({
    root: {
      display: 'grid',
      marginBottom: '8em',
      gridGap: '2em',
      gridTemplateColumns: '1fr 1fr',
      gridTemplateAreas: "'title title' 'image description'",
      [theme.breakpoints.down('md')]: {
        gridTemplateColumns: '1fr',
        gridTemplateAreas: "'title' 'image' 'description' !important",
      },

      '&:nth-child(even)': {
        gridTemplateAreas: "'title title' 'description image'",
      },
    },
    title: {
      gridArea: 'title',
      width: '100%',
      textAlign: 'center',
      marginBottom: '0.7em',
    },
    imageContainer: {
      gridArea: 'image',
    },
    image: {
      position: 'relative',
      borderRadius: '4px',
      overflow: 'hidden',
    },
    description: {
      gridArea: 'description',
      textAlign: 'justify',
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
  }),
  { name: 'boardImageGroup' }
)

const BoardImageGroup = ({
  title,
  alt,
  image,
  ratioX,
  ratioY,
  children,
  className,
  priority,
  ...props
}) => {
  const classes = useStyles()
  const theme = useTheme()

  return (
    <div className={[classes.root, className].join(' ')} {...props}>
      {title && (
        <Typography variant="h3" className={classes.title}>
          {title}
        </Typography>
      )}
      <div className={classes.imageContainer}>
        <Image
          objectFit="contain"
          className={classes.image}
          src={image}
          alt={alt}
          ratioX={ratioX}
          ratioY={ratioY}
          sizes={`(max-width: ${theme.breakpoints.values.md}px) 100vw, 50vw`}
          priority={priority}
        />
      </div>
      <div className={classes.description}>{children}</div>
    </div>
  )
}

BoardImageGroup.propTypes = {
  /** Title visible above the image group */
  title: PropTypes.string,
  /** alt tag for image components */
  alt: PropTypes.string,
  /** URL to the image */
  image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  /** ratio of the image */
  ratioX: PropTypes.number,
  ratioY: PropTypes.number,
  /** Description element(s) */
  children: PropTypes.node,
  /** Priority=true will load first */
  priority: PropTypes.bool,
  /** @ignore */
  className: PropTypes.string,
}

BoardImageGroup.defaultProps = {
  priority: false,
}

export default BoardImageGroup
