import React, { useEffect } from 'react'
import { FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@mui/styles'
import NoSsr from '@mui/material/NoSsr'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import WarningIcon from '@mui/icons-material/Warning'
import Alert from '@mui/material/Alert'
import { Spinner } from 'amiv-react-components'

import {
  authLoginStart,
  authLoginBySearchParameters,
  authLoginByLocalStorageToken,
} from 'lib/store/auth/actions'

import SEO from './seo'
import Header from './header'

const Footer = dynamic(() => import('./footer'))

const useStyles = makeStyles(
  theme => ({
    '@global *': {
      transition: 'background .125s ease',
    },
    '@global a': {
      color: theme.palette.secondary.main,
    },
    root: {
      width: '100%',
      minHeight: '100vh',
      display: 'grid',
      gridTemplateRows: 'auto auto 1fr auto',
      gridTemplateAreas: "'header' 'jswarning' 'content' 'footer'",
      background: theme.palette.background.default,
    },
    header: {
      marginBottom: 5,
      gridArea: 'header',
      display: 'block',
      width: '100%',
    },
    jsWarning: {
      gridArea: 'jswarning',
      padding: '1em 0',

      '& > *': {
        maxWidth: theme.shape.maxContentWidth,
        padding: '0 1em',
        margin: '0 auto',
      },
    },
    content: {
      gridArea: 'content',
      position: 'relative',
      width: '100%',
      '& > *': {
        maxWidth: theme.shape.maxContentWidth,
        padding: '0 1em',
        margin: '0 auto',
      },
    },
    authenticatedSpinner: {
      width: '100%',
    },
    authenticatedErrorContainer: {
      textAlign: 'center',
      padding: '5em 0',
      '& h1': {
        marginBottom: '1em',
      },
      '& p': {
        fontSize: '1.5em',
      },
    },
    footer: {
      gridArea: 'footer',
      width: '100%',
    },
  }),
  { name: 'layout' }
)

const Layout = ({
  className,
  seoProps,
  authenticatedOnly,
  authenticatedReason,
  children,
}) => {
  const classes = useStyles()
  const { isLoggedIn, isPending, isLocalStorageLoaded } = useSelector(
    state => state.auth
  )
  const dispatch = useDispatch()
  const theme = useTheme()

  const { locale } = useRouter()

  // [auth] Load token from localStorage if available
  useEffect(() => {
    if (!isLocalStorageLoaded) {
      dispatch(authLoginByLocalStorageToken())
    }
  })

  // [auth] Try to log in with token from search params if available
  useEffect(() => {
    const searchString = window.location.search
    if (searchString) {
      dispatch(authLoginBySearchParameters(new URLSearchParams(searchString)))
    }
  })

  let content = children

  if (locale === 'default') {
    content = null
  } else if (authenticatedOnly && !isLoggedIn) {
    if (isPending) {
      content = (
        <div className={classes.authenticatedSpinner}>
          <Spinner
            centered
            size={64}
            background={theme.palette.common.white}
            elevation={2}
          />
        </div>
      )
    } else {
      content = (
        <div className={classes.authenticatedErrorContainer}>
          <h1>
            <FormattedMessage id="error.title" />
          </h1>
          <p>
            {authenticatedReason || <FormattedMessage id="error.403.text" />}
          </p>
          <NoSsr>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => dispatch(authLoginStart())}
            >
              <FormattedMessage id="login" />
            </Button>
          </NoSsr>
        </div>
      )
    }
  }

  return (
    <div className={classes.root}>
      {/*
       CssBaseline kickstart an elegant, consistent, and simple baseline to build upon.
       This line must not be in themeContext! Otherwise, server-side rendering does not work correctly.
      */}
      <CssBaseline />
      <Header className={classes.header} />
      <SEO {...seoProps} />
      <noscript>
        <div className={classes.jsWarning}>
          <Alert icon={<WarningIcon />} severity="error">
            <FormattedMessage id="error.javascriptDisabled" />
          </Alert>
        </div>
      </noscript>
      <main className={[classes.content, className].join(' ')}>{content}</main>
      <Footer className={classes.footer} />
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  seoProps: PropTypes.object.isRequired,
  authenticatedOnly: PropTypes.bool,
  authenticatedReason: PropTypes.node,
}

Layout.defaultProps = {
  authenticatedOnly: false,
}

export default Layout
