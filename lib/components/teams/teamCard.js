import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles, useTheme } from '@mui/styles'
import { Card, CardContent, Typography, CardActions } from '@mui/material'

import amivWheel from 'public/logos/amiv-wheel.svg'
import contactIconMapping from 'content/teams/contactIconMapping'
import TranslatedContent from '../general/translatedContent'
import Image from '../general/image'

const useStyles = makeStyles(
  theme => ({
    root: {
      height: '100%',
      display: 'grid',
      gridTemplateRows: 'auto 1fr auto',
    },
    image: {
      height: 225,
      backgroundColor: theme.palette.common.grey,
    },
    description: {
      textAlign: 'justify',
      fontSize: '1.1em',
    },
    actionArea: {
      padding: '16px 0',
      flexWrap: 'wrap',
      textAlign: 'left',
      borderTop: `1px solid ${theme.palette.common.grey}`,
    },
    action: {
      width: '100%',
      margin: '0 !important',
      padding: '.5em 1em',
      display: 'flex',
      alignItems: 'center',
      textDecoration: 'none',
      color: theme.palette.text.secondary,
    },
    actionIcon: {
      marginRight: '.5em',
    },
    actionLabel: {
      flexGrow: 1,
      fontSize: '1.2em',
    },
  }),
  { name: 'teamCard' }
)

const TeamCard = ({ team, className, ...props }) => {
  const classes = useStyles()
  const theme = useTheme()

  return (
    <Card className={[classes.root, className].join(' ')} {...props}>
      <div>
        <Image
          className={classes.image}
          objectFit="cover"
          sizes={`(max-width: ${theme.breakpoints.values.sm}px) 100vw, (max-width: ${theme.breakpoints.values.lg}px) 50vw, 33vw`}
          src={team.image || amivWheel}
          alt={team.name}
          ratioX={16}
          ratioY={9}
        />
      </div>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {team.name}
        </Typography>
        <TranslatedContent
          className={classes.description}
          content={team.description}
          parseMarkdown
        />
      </CardContent>
      <CardActions className={classes.actionArea}>
        {team.contact &&
          team.contact.map((item, index) => {
            const isMultiLangLabel = item.label && !item.label._
            const ItemIcon = contactIconMapping[item.icon]
            return (
              <a
                key={index}
                className={classes.action}
                href={item.url}
                target="_blank"
                rel="noopener noreferrer"
              >
                {ItemIcon && <ItemIcon className={classes.actionIcon} />}
                {isMultiLangLabel ? (
                  <TranslatedContent
                    noHint
                    className={classes.actionLabel}
                    content={item.label}
                  />
                ) : (
                  <Typography className={classes.actionLabel} variant="body1">
                    {item.label._}
                  </Typography>
                )}
              </a>
            )
          })}
      </CardActions>
    </Card>
  )
}

TeamCard.propTypes = {
  /** Team object */
  team: PropTypes.object.isRequired,
  /** @ignore */
  className: PropTypes.string,
}

export default TeamCard
