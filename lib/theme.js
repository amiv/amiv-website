import { red, grey } from '@mui/material/colors'
import { createTheme as createMuiTheme } from '@mui/material/styles'

export const defaultTheme = 'light'

export const createTheme = type => {
  const isDark = type === 'dark'

  return createMuiTheme({
    typography: { fontSize: 12.5, button: { fontSize: '0.95rem' } },
    breakpoints: { values: { xs: 0, sm: 540, md: 970, lg: 1080, xl: 1280 } },
    shape: { maxContentWidth: 1280, headerHeight: 88, submenuHeight: '3em' },
    palette: {
      text: { hint: 'rgba(0, 0, 0, 0.38)' },
      mode: type,
      type,
      common: {
        white: '#fff',
        black: '#000',
        grey: isDark ? grey[700] : grey[200],
        greyContrastText: isDark ? '#fff' : '#000',
        amivblue: '#1f2d54',
        amivred: '#e8462b',
        orange: '#e8a92b',
        orangeContrastText: '#fff',
        mobileSubmenu: isDark ? grey[500] : '#fff',
        header: isDark ? grey[900] : '#1f2d54',
        headerContrastText: '#fff',
      },
      primary: { main: isDark ? '#fff' : '#1f2d54' },
      secondary: { main: '#e8462b' },
      info: { main: grey[700], contrastText: '#fff' },
      error: { main: red.A400 },
      background: { default: isDark ? '#333' : '#fff' },
    },
    components: {
      MuiPaper: {
        styleOverrides: {
          root: {
            background: isDark ? grey[800] : '#fff',
          },
        },
      },
    },
  })
}
