import { NextResponse } from 'next/server'
import nextConfig from 'next.config'

const PUBLIC_FILE = /\.([a-zA-Z0-9]+$)/

export function middleware(request) {
  const pathName = request.nextUrl.pathname

  const shouldHandleLocale =
    !PUBLIC_FILE.test(pathName) &&
    !pathName.includes('_next/') &&
    !pathName.includes('/media/') &&
    request.nextUrl.locale === 'default'

  if (shouldHandleLocale) {
    const locales = nextConfig.i18n.locales.filter(
      locale => locale !== 'default'
    )
    const prefLocales =
      request.headers
        .get('accept-language')
        ?.split(',')
        ?.map(item => item.split(';', 1)[0].split('-')[0]) || []

    // Find first preferred language which is configured for the application.
    // If no match was found, the first real language is taken instead.
    const locale =
      prefLocales.find(locale1 =>
        locales.some(locale2 => locale1 === locale2)
      ) || locales[0]
    const newNextUrl = request.nextUrl.clone()

    if (newNextUrl.pathname === '/') {
      newNextUrl.pathname = `/${locale}`
    } else {
      newNextUrl.locale = locale
    }

    return NextResponse.redirect(newNextUrl)
  }

  return undefined
}
