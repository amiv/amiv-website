import React from 'react'

import Layout from 'lib/components/layout'
import TranslatedContent from 'lib/components/general/translatedContent'
import content_en from 'content/about.en.md'
import content_de from 'content/about.de.md'

const AboutPage = () => {
  return (
    <Layout seoProps={{ title: 'about.title' }}>
      <TranslatedContent
        content={{ en: content_en, de: content_de }}
        noEscape
      />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default AboutPage
