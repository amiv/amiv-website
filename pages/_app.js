import React from 'react'
import PropTypes from 'prop-types'
import { Provider as ReduxProvider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { useRouter } from 'next/router'
import Head from 'next/head'
import CssBaseline from '@mui/material/CssBaseline'
import StyledEngineProvider from '@mui/material/StyledEngineProvider'
import { CacheProvider } from '@emotion/react'

// import all locales through barrel file
import * as locales from 'locales'
import { wrapper } from 'lib/store/createStore'
import { ThemeProvider } from 'lib/context/themeContext'
import createEmotionCache from 'lib/createEmotionCache'

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache()

const MyApp = ({
  Component,
  // eslint-disable-next-line react/prop-types
  emotionCache = clientSideEmotionCache,
  ...rest
}) => {
  const router = useRouter()
  const { store, props } = wrapper.useWrappedStore(rest)
  const { pageProps } = props
  const { locale } = router
  const messages = { ...locales.en, ...locales[locale] }

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles?.parentElement?.removeChild(jssStyles)
    }
  }, [])

  // if (locale === 'en') {
  //   locale = 'en-GB'
  // }

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <StyledEngineProvider injectFirst>
        <ThemeProvider>
          <IntlProvider locale={locale} defaultLocale="en" messages={messages}>
            <ReduxProvider store={store}>
              {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
              <CssBaseline />
              <Component {...pageProps} />
            </ReduxProvider>
          </IntlProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </CacheProvider>
  )
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
}

export default MyApp
