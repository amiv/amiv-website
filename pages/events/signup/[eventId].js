import { wrapper } from 'lib/store/createStore'
import { loadItem } from 'lib/store/common/actions'
import { EVENTS } from 'lib/store/events/constants'
import { SignUpPage } from 'lib/components/events/signUpPage'

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 600s (10min).
export const getStaticProps = wrapper.getStaticProps(
  store =>
    async ({ params }) => {
      try {
        await store.dispatch(loadItem(EVENTS, { id: params.eventId })) // params.eventId comes from [eventId] in the file name
      } catch (err) {
        return { notFound: true }
      }
      return { props: {}, revalidate: 600 }
    }
)

export const getStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' }
}

export default SignUpPage
