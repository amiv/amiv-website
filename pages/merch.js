import React from 'react'

import Layout from 'lib/components/layout'
import TranslatedContent from 'lib/components/general/translatedContent'
import MerchItem from 'lib/components/merch/merchItem'
import MerchCarousel from 'lib/components/merch/merchCarousel'

import content_en from 'content/merch/merch.en.md'
import content_de from 'content/merch/merch.de.md'

const extractName = key => {
  const result = key.match(/\.\/([^-]*)(-[0-9])?\.[a-zA-Z]+$/)
  if (result && result.length > 1) {
    return result[1]
  }
  return key
}

const merchData = (() => {
  const data = require.context(
    'content/merch/products',
    false,
    /^.\/[0-9]+.*.json$/
  )
  const images = require.context(
    'content/merch/products',
    false,
    /^.\/[0-9]+.*.(jpg|JPG|png|PNG)$/
  )

  const getImages = name => {
    return images
      .keys()
      .filter(item => extractName(item) === name)
      .map(key => images(key))
  }

  return data.keys().map(item => {
    const name = extractName(item)
    return { ...data(item), key: name, images: getImages(name) }
  })
})()

const MerchPage = () => {
  return (
    <Layout seoProps={{ title: 'merch.title' }}>
      <TranslatedContent
        content={{ en: content_en, de: content_de }}
        noEscape
      />

      <MerchCarousel data={merchData} />

      <div style={{ marginBottom: '20px' }}>
        {merchData.map(item => (
          <MerchItem item={item} key={item.key} />
        ))}
      </div>
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default MerchPage
