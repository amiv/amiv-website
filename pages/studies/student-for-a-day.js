import React from 'react'
import { useIntl } from 'react-intl'
import { makeStyles } from '@mui/styles'

import Layout from 'lib/components/layout'
import Image from 'lib/components/general/image'
import TranslatedContent from 'lib/components/general/translatedContent'
import featuredImage from 'content/studies/student-for-a-day.jpg'
import content_de from 'content/studies/student-for-a-day.de.md'
import content_en from 'content/studies/student-for-a-day.en.md'

const useStyles = makeStyles(
  theme => ({
    image: {
      marginBottom: '2em',
      height: 225,
      backgroundColor: theme.palette.common.grey,
    },
  }),
  { name: 'studentForADay' }
)

const StudentForADayPage = () => {
  const intl = useIntl()
  const classes = useStyles()

  return (
    <Layout seoProps={{ title: 'mainMenu.studies.studentForADay' }}>
      <TranslatedContent
        content={{ de: content_de, en: content_en }}
        noEscape
      />
      <Image
        className={classes.image}
        layout="responsive"
        src={featuredImage}
        alt={intl.formatMessage({ id: 'mainMenu.studies.studentForADay' })}
        ratioX={8.9}
        ratioY={3}
      />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default StudentForADayPage
