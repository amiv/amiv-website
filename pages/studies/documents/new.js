import React from 'react'

import StudydocumentsEditPage from 'lib/components/studies/documents/editPage'

const StudydocumentsNewPage = () => {
  return <StudydocumentsEditPage />
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default StudydocumentsNewPage
