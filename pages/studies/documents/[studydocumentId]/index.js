import React from 'react'
import { useRouter } from 'next/router'

import StudydocumentsListPage from 'lib/components/studies/documents/listPage'

const StudydocumentsDetailPage = () => {
  const {
    query: { studydocumentId },
  } = useRouter()

  return <StudydocumentsListPage studydocumentId={studydocumentId} />
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export const getStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' }
}

export default StudydocumentsDetailPage
