import React from 'react'
import { useRouter } from 'next/router'

import StudydocumentsEditPage from 'lib/components/studies/documents/editPage'

const StudydocumentsDetailEditPage = () => {
  const {
    query: { studydocumentId },
  } = useRouter()

  return <StudydocumentsEditPage studydocumentId={studydocumentId} />
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export const getStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' }
}

export default StudydocumentsDetailEditPage
