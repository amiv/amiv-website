import React from 'react'

import StudydocumentsListPage from 'lib/components/studies/documents/listPage'

const StudydocumentsPage = () => {
  return <StudydocumentsListPage />
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default StudydocumentsPage
