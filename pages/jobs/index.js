import React from 'react'

import JobsListPage from 'lib/components/jobs/listPage'
import { wrapper } from 'lib/store/createStore'
import { JOBOFFERS } from 'lib/store/joboffers/constants'
import { listLoadAllPages } from 'lib/store/common/actions'

const JobsPage = () => {
  return <JobsListPage />
}

// Ensures that the required data to render the page is fetched before rendering
// the page during build-time and on server-side.
// Page rendering is automatically re-generated after 600s (10min).
export const getStaticProps = wrapper.getStaticProps(store => async () => {
  await store.dispatch(listLoadAllPages(JOBOFFERS, { listName: 'default' }))

  return { props: {}, revalidate: 600 }
})

export default JobsPage
