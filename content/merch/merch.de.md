# AMIV Merch

Wusstest Du, dass du noch nie einfacher an AMIV-Merch kommen konntest?

Komm einfach im Büro (CAB E37) vorbei, probier ihn an, lass dich professionell beraten (von deinen Kommilitonen) und kaufe dir deine Lieblings-Pieces! Jetzt auch per TWINT! Karten- oder Barzahlung ist natürlich auch möglich :)
