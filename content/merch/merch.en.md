# AMIV Merch

Did you know that AMIV merch has never been easier to get your hands on?

Just drop by the office (CAB E37), try it on, get professional fashion advice (from your fellow students) and buy your favorite pieces! Now also via TWINT! Card or cash payment is of course also possible :)
