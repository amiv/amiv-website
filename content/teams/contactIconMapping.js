import WebsiteIcon from '@mui/icons-material/Public'
import EmailIcon from '@mui/icons-material/Email'
import PhoneIcon from '@mui/icons-material/Phone'
import CalendarIcon from '@mui/icons-material/Event'
import InstagramIcon from '@mui/icons-material/Instagram'
import FacebookIcon from '@mui/icons-material/Facebook'
import LinkedInIcon from '@mui/icons-material/LinkedIn'

/**
 * This specifies a mapping of the string value to the actual icon
 *
 * Have a look at https://material-ui.com/components/material-icons/ for more icons!
 */
const contactIconMapping = {
  phone: PhoneIcon,
  email: EmailIcon,
  website: WebsiteIcon,
  calendar: CalendarIcon,
  instagram: InstagramIcon,
  facebook: FacebookIcon,
  linkedin: LinkedInIcon,
}

export default contactIconMapping
