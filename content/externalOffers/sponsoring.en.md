# Offers of AMIV at ETH

### WELCOME TO AMIV!

Here we present various opportunities for you to make your company and your industry known among students.

The Academic Mechanical and Electrical Engineering Association (AMIV) at ETH is a professional association representing students of Electrical Engineering and Information Technology, Mechanical Engineering, as well as Process Engineering, Micro and Nano Systems, Robotics, Systems and Control, Nuclear Engineering, Biomedical Engineering, Integrated Building Systems, Neural Systems and Computation, and Energy Science and Technology. With about 4400 members, it is by far the largest and most active professional society at ETH.

More details about our offerings can be found in our sponsorship brochure:
