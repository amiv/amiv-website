# Angebote des AMIV an der ETH

### WILLKOMMEN BEIM AMIV!

Hier stellen wir Ihnen diverse Möglichkeiten vor, wie Sie Ihr Unternehmen und Ihre Branche unter den Studierenden bekannt machen können.

Der Akademische Maschinen- und Elektro-Ingenieur Verein (AMIV) an der ETH vertritt als Fachverein die Studentinnen und Studenten der Studiengänge Elektrotechnik und Informationstechnologie, Maschinenbau, sowie Verfahrenstechnik, Mikro- und Nanosysteme, Robotics, Systems and Control, Nuclear Engineering, Biomedical Engineering, Integrated Building Systems, Neural Systems and Computation und Energy Science and Technology. Mit ca. 4400 Mitgliedern ist er mit Abstand der größte und aktivste Fachverein an der ETH.

Mehr Details zu unseren Angeboten finden Sie in unserer Sponsoring-Broschüre:
