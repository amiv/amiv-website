# Student for a Day

Du bist dir noch nicht sicher, was du studieren möchtest? Willst du wissen, wie ein typischer Tag für Studierende an der ETH verläuft?

Dann bist du bei uns genau richtig! Wir bieten dir die Möglichkeit, für einen Tag in das Elektrotechnik- und/oder Maschinenbaustudium zu schnuppern. Dabei wirst du eine:n Student:in in die Vorlesungen und Übungen begleiten und kannst dir all deine Fragen zum Studium beantworten lassen.

So kannst du dich für einen Tag bereits wie eine Student:in fühlen und bekommst einen Eindruck, was dich in Studium an der ETH erwartet.

## Anmeldung

Wenn du Lust auf den "Student for a Day" Schnuppertag hast, dann melde dich unverbindlich an. Je nach deinen Vorlesungsinteressen werden wir dann gemeinsam einen Termin ausmachen. Bitte habe Verständnis, falls wir uns nicht sofort bei dir melden. Manchmal ist es im Studium etwas stressig. Du erreichst uns bei Fragen unter studentforaday@amiv.ethz.ch.

<a href="https://docs.google.com/forms/d/e/1FAIpQLSdIHKBMnIC-MQvjYEHu3MnenfeEb3vumeE_YnNSV27SyeIslg/viewform?usp=sf_link" target="_blank">Zum Anmeldeformular</a>
