# Regeln

1. Diese Plattform lebt vom geben und nehmen - investiere also auch mal ein paar Minuten und schaue ob du für andere nützliches Material hast und lade es hoch. Es kostet nicht viel Zeit.

2. Einige der hier zu findenden Daten unterliegen der [BOT](https://rechtssammlung.sp.ethz.ch/_layouts/15/start.aspx#/default.aspx) sowie dem Urheberrecht und dienen zur internen Dokumentation gemäss [Bundesgesetz SR 231.1, Art. 19.1c](https://www.admin.ch/opc/de/classified-compilation/19920251/index.html#a19) und dürfen nicht an Nicht-ETH-Angehörige weitergegeben werden - daher ist der Login verpflichtend.

3. Plagiate können schwerwiegende Folgen haben. Gib also keine Werke von anderen als dein eigenes aus und lies dir [diese Seite](https://www.ethz.ch/studierende/de/studium/leistungskontrollen/plagiate.html) durch.

4. Hast du Feedback für uns oder ein Problem entdeckt? Sag uns Bescheid unter info@amiv.ethz.ch.

Wir bedanken uns im Namen aller Studierenden bei Allen, die ihre Unterlagen, Zusammenfassungen, etc. hier für andere Studierende zugänglich machen. Danke tausend, Gruss und Kuss.
