# Student for a Day

Still not sure what you want to study? 
Do you want to find out what a typical day is like for students at ETH?

Then you've come to the right place! 
We offer you the opportunity to get a taste of studying electrical engineering and/or mechanical engineering for a day. 
You will accompany a student to lectures and exercises and can have all your questions about studying answered.

This way, you can already feel like a student for a day and get an impression of what awaits you in your studies at ETH.

## Registration

If you are interested in "Student for a Day", then register without obligation.
Depending on your lecture interests, we will arrange a date together.
Please understand if we don't get back to you straight away.
Sometimes studying can be a bit stressful.
If you have any questions, you can reach us at studentforaday@amiv.ethz.ch.

<a href="https://docs.google.com/forms/d/e/1FAIpQLSdIHKBMnIC-MQvjYEHu3MnenfeEb3vumeE_YnNSV27SyeIslg/viewform?usp=sf_link" target="_blank">To the registration form</a>
