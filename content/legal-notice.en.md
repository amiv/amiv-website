# Legal Notice

## Contact / Postal address

**AMIV an der ETH**</br>
CAB E37</br>
Universitätstrasse 6</br>
8092 Zürich</br>
Switzerland</br>

[info@amiv.ethz.ch](mailto:info@amiv.ethz.ch)</br>
[+41 (0)44 / 632 64 67](tel:+41446326467)

## Disclaimer

The author assumes no liability whatsoever with regard to the correctness, accuracy, up-to-dateness, reliability and completeness of the information provided.

Liability claims against the author for damages of a material or immaterial nature arising from access to or use or non-use of the published information, from misuse of the connection or from technical faults are excluded.

All offers are non-binding. The author expressly reserves the right to change, supplement or delete parts of the pages or the entire offer without special notice or to temporarily or permanently cease publication.

References and links to third-party websites are outside our area of responsibility. Any responsibility for such websites is rejected. Access to and use of such websites is at the user's own risk.

## Copyright

The copyright and any other rights relating to texts, illustrations, photos or any other files on the website belong exclusively to the entity «AMIV an der ETH» or mentioned owners. Any reproduction requires the written permission of the copyright holder , which must be obtained in advance.
